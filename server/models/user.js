'use strict';
const bcrypt = require('../helpers/bcrypt')

module.exports = (sequelize, DataTypes) => {
  const models = sequelize.Sequelize.Model
  class User extends models { }

  User.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Name must be filled'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: 'Must be filled with email'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Password must be filled'
        }
      }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Role must be filled'
        }
      }
    },
    image_id: {
      type: DataTypes.STRING,
      allowNull: true,
      validate:{
        isNumeric: {
          msg: 'must be filled by integer'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Users',
    underscored: true,
    hooks: {
      beforeCreate: (user, options) => {
        user.password = bcrypt.hasher(user.password)
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
    User.hasMany(models.Todo, {
      foreignKey: 'user_id',
      sourceKey: 'id'
    }),
    User.hasOne(models.Image, {
      foreignKey: 'image_id',
      sourceKey: 'id'
    })
  };
  return User;
};