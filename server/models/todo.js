'use strict';
module.exports = (sequelize, DataTypes) => {
  const models = sequelize.Sequelize.Model
  class Todo extends models { }

  Todo.init({
    task: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Task Must be Filled'
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Description Must be Filled'
        }
      }
    },
    due_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Due Date Must be Filled'
        },
        isDate: {
          msg: 'Due Date Must be Filled by Date'
        },
        isAfter: `${new Date()}`
      }
    },
    importance: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      validate: {
        notEmpty: {
          msg: 'Importance Must be Filled'
        }
      }
    },
    completion: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      validate: {
        notEmpty: {
          msg: 'Completion Must be Filled'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'User ID Must be Filled'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Todos',
    underscored: true
  })

  Todo.associate = function (models) {
    // associations can be defined here
    Todo.belongsTo(models.User, { 
      foreignKey: 'user_id',
      targetKey: 'id' 
    })
  };
  return Todo;
};