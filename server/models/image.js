'use strict';
module.exports = (sequelize, DataTypes) => {
  const models = sequelize.Sequelize.Model
  class Image extends models { }

  Image.init({
    url_image: {
      type: DataTypes.TEXT,
      validate: {
        isUrl: {
          msg: 'Image must be Filled by URL'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Uploads',
    underscored: true
  })

  Image.associate = function (models) {
    // associations can be defined here
    Image.belongsTo(models.User, {
      foreignKey: 'image_id',
      targetKey: 'id'
    })
  };
  return Image;
};