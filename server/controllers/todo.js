const { Todo } = require('../models')

exports.create = async function (req, res, next) {
    try {
        let todo = await Todo.create({
            task: req.body.task,
            description: req.body.description,
            due_date: req.body.due_date,
            importance: req.body.importance,
            completion: req.body.completion,
            user_id: req.payload.data.id
        });
        res.status(200);
        res.data = { todo };
        next();
    }

    catch (err) {
        res.status(422);
        next(err);
    };
}

exports.findAll = async function (req, res, next) {
    try {
        let todos = await Todo.findAll({
            where: {
                user_id: req.payload.data.id
            },
            limit: 10
        })
        res.status(200);
        res.data = { todos };
        next()
    } catch (err) {
        res.status(404);
        next(err);
    }
}

exports.findUserTodo = async function (req, res, next) {
    try {
        const todos = await Todo.findAll({
            where: {
                user_id: req.params.id
            },
            limit: 10
        })
        res.status(200);
        res.data = { todos };
        next()
    } catch (err) {
        res.status(404);
        next(err);
    }
}

exports.update = async function (req, res, next) {

    try {
        let { task, description, due_date, importance, completion } = req.body

        let dataUpdate = {
            task,
            description,
            due_date,
            importance,
            completion
        }

        await Todo.update(dataUpdate, {
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully updated!";
        next();
    } catch (err) {
        res.status(422);
        next(err);
    };
}

exports.delete = async function (req, res, next) {
    try {
        await Todo.destroy({
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully deleted!";
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}

exports.updateAdmin = async function (req, res, next) {

    try {
        let { task, description, due_date, importance, completion } = req.body

        let dataUpdate = {
                task,
                description,
                due_date,
                importance,
                completion
        }

        await Todo.update(dataUpdate, {
            where: { id: req.params.id }
        });
        res.status(200);
        res.data = "Successfully updated!";
        next();
    } catch (err) {
        res.status(422);
        next(err);
    };
}

exports.deleteAdmin = async function (req, res, next) {
    try {
        await Todo.destroy({
            where: {
                id: req.params.id
            }
        });
        res.status(200);
        res.data = "Successfully deleted!";
        next();
    } catch (err) {
        res.status(500);
        next(err);
    };
}