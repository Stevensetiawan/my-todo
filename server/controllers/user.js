const { User, Image } = require('../models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const bcrypt = require('../helpers/bcrypt')
const jwt = require('../helpers/jwt')
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT);
const imagekit = require('../lib/imagekit');

module.exports = {
    async register(req, res, next) {
        // lakukan create
        let { name, email, password } = req.body
        try {
            let isChecking = await User.findOne({
                where: {
                    email
                }
            })
            if (!isChecking) {
                let user = await User.create({
                    name,
                    email,
                    password,
                    role: "member"
                })
                let access_token = jwt.jwtSign({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    role: user.role
                })
                res.status(201);
                res.data = { access_token }
                next()
            } else {
                throw new Error('Email already exist')
            }
        } catch (err) {
            res.status(422)
            next(err)
        }
    },

    async login(req, res, next) {
        console.log(req.body)
        let { email, password } = req.body
        try {
            let user = await User.findOne({
                where: {
                    email
                }
            })
            if (user && user.role !== 'admin') {
                let isValidate = bcrypt.checker(password, user.password)
                if (isValidate) {
                    let access_token = jwt.jwtSign({
                        id: user.id,
                        email: user.email,
                        role: user.role
                    })
                    res.status(201);
                    res.data = { access_token }
                    next()
                }
            } else {
                res.status(404).json({ msg: "id or email is not found" })
            }

        } catch (err) {
            res.status(401);
            next(err);
        }
    },

    async adminLogin(req, res, next) {
        console.log(req.body)
        let { email, password } = req.body

        try {
            let admin = await User.findOne({
                where: {
                    email
                }
            })
            if (admin) {
                let isValidate = bcrypt.checker(password, admin.password)
                if (isValidate) {
                    let access_token = jwt.jwtSign({
                        id: admin.id,
                        email: admin.email,
                        role: admin.role
                    })
                    res.status(201);
                    res.data = { access_token }
                    next()
                }
            } else {
                res.status(404).json({ msg: "id or email is not found" })
            }

        } catch (err) {
            res.status(401);
            next(err);
        }
    },

    async userUpdate(req, res, next) {
            let { name, email, role } = req.body

            try {
                let user = await User.update({
                    name,
                    email,
                    role
                }, {
                    where: {
                        id: req.params.id
                    }
                })
                if (user) {
                    res.status(200);
                    res.data = "Successfully updated!";
                    next();
                } else {
                    throw new Error('Update is fail')
                }
            } catch (err) {
                res.status(422);
                next(err);
            }
    },

    async uploadImageAdmin(req, res, next) {
        const split = req.file.originalname.split('.');
        const ext = split[split.length - 1];
        try {
            var image = await imagekit.upload({
                file: req.file.buffer,
                fileName: `IMG-${Date.now()}.${ext}`
            })
            let imageExist = await User.findOne({
                where: {
                    id: req.params.id
                }
            })

            if (imageExist) {
                await Image.update({
                    image_url: image.url
                })
                res.status(200);
                res.data = "Successfully updated!";
                next();
            } else {
                let imageDetail = await Image.create({
                    image_url: image.url
                })

                await User.update({
                    image_id: imageDetail.id
                })
                res.status(200);
                res.data = "Successfully created!";
                next();
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async uploadImageUser(req, res, next) {
        const split = req.file.originalname.split('.');
        const ext = split[split.length - 1];
        try {
            var image = await imagekit.upload({
                file: req.file.buffer,
                fileName: `IMG-${Date.now()}.${ext}`
            })
            let imageExist = await User.findOne({
                where: {
                    id: req.payload.data.id
                }
            })

            if (imageExist) {
                await Image.update({
                    image_url: image.url
                })
                res.status(200);
                res.data = "Successfully updated!";
                next();
            } else {
                let imageDetail = await Image.create({
                    image_url: image.url
                })

                await User.update({
                    image_id: imageDetail.id
                })
                res.status(200);
                res.data = "Successfully created!";
                next();
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminUpdate(req, res, next) {
        let { name, email, role } = req.body

        try {
            let user = await User.update({
                name,
                email,
                role
            }, {
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = "Successfully updated!";
                next();
            } else {
                throw new Error('Update is fail')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminDelete(req, res, next) {
        try {
            let user = await User.destroy({
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = "Successfully deleted!";
                next();
            } else {
                throw new Error('Delete is fail')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminFindOne(req, res, next) {
        try {
            let user = await User.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminFindAll(req, res, next) {
        try {
            let user = await User.findAll({
                limit: 10,
                order: [
                    ['email', 'ASC'],
                ]
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async adminSearchUser(req, res, next) {
        try {
            let user = await User.findAll({
                where: {
                    email: {
                        [Op.iLike]: `%${req.body.email}%`
                    }
                },
                limit: 10,
                order: [
                    ['email', 'ASC'],
                ]
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async googleLogin(req, res, next) {
        client.verifyIdToken({
            idToken: req.body.id_token,
            audience: process.env.CLIENT,  // Specify the CLIENT_ID of the app that accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
        }).then(ticket => {
            const payload = ticket.getPayload();
            User.findOne({
                where: {
                    email: payload.email
                }
            })
                .then(data => {
                    if (data) {
                        return data
                    }
                    else {
                        let obj = {
                            email: payload.email,
                            password: "apapun",
                            role: 'member'
                        }
                        return User.create(obj)
                    }
                })
                .then(data => {
                    // console.log(data)
                    if (data) {
                        var access_token = jwt.jwtSign(data)
                    }
                    res.status(200).json({ access_token })
                })
                .catch(err => {
                    next(err)
                })
        })


    }
}