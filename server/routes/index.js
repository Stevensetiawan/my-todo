const router = require('express').Router()
const todo = require('./todo')
const user = require('./user')
const admin = require('./admin')

router.use('/todo',todo)
router.use('/user',user)
router.use('/admin',admin)

module.exports= router

