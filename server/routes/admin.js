const router = require('express').Router()
const user = require('../controllers/user')
const success = require('../middlewares/success')
const authentication = require('../middlewares/authentication')
const isAdmin = require('../middlewares/isAdmin')
const uploader = require('../middlewares/uploader');

router.post('/login',user.adminLogin,success)

router.use(authentication)
router.use(isAdmin)
router.get('/findUser/:id',user.adminFindOne,success)
router.post('/searchUser',user.adminSearchUser,success)
router.get('/findAllUser',user.adminFindAll,success)
router.put('/update/:id',user.adminUpdate,success)
router.put('/updateImage/:id',uploader.single('image'),user.uploadImageAdmin,success)
router.delete('/delete/:id',user.adminDelete,success)

module.exports=router