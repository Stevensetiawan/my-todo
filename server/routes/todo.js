const router = require('express').Router()
const todo = require('../controllers/todo')
const authentication = require('../middlewares/authentication')
const authorization = require('../middlewares/authorization')
const isAdmin = require('../middlewares/isAdmin')
const success = require('../middlewares/success')

router.use(authentication)
router.get('/findAll',todo.findAll,success)
router.post('/create',todo.create,success)
router.put('/update/:id',authorization,todo.update,success)
router.delete('/delete/:id',authorization,todo.delete,success)

router.use(isAdmin)
router.get('/findUserTodo/:id',todo.findUserTodo,success)
router.put('/updateAdmin/:id',todo.updateAdmin,success)
router.put('/deleteAdmin/:id',todo.deleteAdmin,success)


module.exports=router