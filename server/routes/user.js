const router = require('express').Router()
const user = require('../controllers/user')
const authentication = require('../middlewares/authentication')
const authorization = require('../middlewares/authorization')
const uploader = require('../middlewares/uploader');
const success = require('../middlewares/success')

router.post('/register',user.register,success)
router.post('/login',user.login,success)
router.post('/googleLogin',user.googleLogin,success)

router.use(authentication)
router.put('/updateProfile/:id',authorization,user.userUpdate,success)
router.put('/updateImage/:id',uploader.single('image'),user.uploadImageUser,success)

module.exports=router