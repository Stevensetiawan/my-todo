const { Todo } = require('../models')
module.exports = (req, res, next) => {

    let id = {
        where: {
            id: req.params.id
        }
    }
    Todo.findOne(id)
        .then(todo => {
            if (todo) {
                if (todo.user_id === req.payload.data.id) {
                    next()
                } else {
                    res.status(401).json({ msg: "unauthorized" })
                }
            } else {
                res.status(404).json({ msg: "data is not found" })
            }
        }).catch(err => {
            res.status(500).json(err)
        })

}