const jwt = require('../helpers/jwt')
const { User } = require('../models')

module.exports = (req, res, next) => {
    let access_token = req.headers.token

    let payload;

    try {
        payload = jwt.jwtVerify(access_token)
    } catch (err) {
        res.status(500).json({
            status: "error",
            errors: [err.message]
        })
    }

    User.findByPk(payload.data.id)
        .then(user => {
            if (user) {
                req.payload = payload
                next()
            } else {
                res.status(404).json({ message: "data is not found" })
            }
        }).catch(err => {
            res.status(500).json({ message: "internal server error" })
        })
}