const multer = require('multer');
const env = process.env.NODE_ENV || 'development';
const upload = multer();

module.exports = upload;