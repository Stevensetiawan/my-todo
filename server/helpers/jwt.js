const jwt = require('jsonwebtoken');

exports.jwtSign = (data) => {
    return jwt.sign({ data }, process.env.SECRET);
}

exports.jwtVerify = (token) => {
    return jwt.verify(token, process.env.SECRET);
}
