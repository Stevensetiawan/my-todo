const request = require('supertest');
const app = require('../app');
const { User } = require('../models');

describe('Posts API Collection', () => {

  beforeAll(() => {
    return User.destroy({
      truncate: true
    })
  })

  afterAll(() => {
    return User.destroy({
      truncate: true
    })
  })

  describe('POST /MyTodo/v1/user/register', () => {

    test('Should succesfully create new register', done => {
      request(app)
        .post('/MyTodo/v1/user/register')
        .set("Content-Type", "multipart/form-data")
        .field({ name: 'test', email: 'test@mail.com', password: 'test' })
        .attach('avatar', 'uploads/avatar.jpg')
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');
          expect(res.body.data.access_token)
            .toEqual(
              "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjoyLCJuYW1lIjoidGVzdDIyIiwiZW1haWwiOiJ0ZXN0MjJAbWFpbC5jb20iLCJpbWFnZSI6IjRiNjlmN2EzMTYzOGRhMWI3MzdmNjliMzA1ZjUwN2E5Iiwicm9sZSI6Im1lbWJlciJ9LCJpYXQiOjE1OTA2MzA4NTh9.GoYMHlhxa3ULpOOVN_vz9lU-LRzED0R-Al2JOWfxh50"
            );
          done();
        })
    })

    test('Should not create new register', done => {
      request(app)
        .post('/MyTodo/v1/user/register')
        .set("Content-Type", "multipart/form-data")
        .field({ email: 'test@mail.com', password: 'test' })
        .then(res => {
          expect(res.statusCode).toEqual(422);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })

  })
})